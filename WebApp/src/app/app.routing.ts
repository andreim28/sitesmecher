import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./services/auth.guard";
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";
import { NgModule } from "@angular/core";
import { AccountRecoveryComponent } from "./components/account-recovery/account-recovery.component";
import { LoginGuard } from "./services/login.guard";
import { ConfirmAccountComponent } from "./components/confirm-account/confirm-account.component";
import { PasswordResetComponent } from "./components/password-reset/password-reset.component";
import { PasswordTestComponent } from "../app/password-test/password-test.component";
const appRoutes: Routes = [
  { path: "", component: HomeComponent, canActivate: [AuthGuard] },
  { path: "login", component: LoginComponent, canActivate: [LoginGuard] },
  { path: "register", component: RegisterComponent, canActivate: [LoginGuard] },
  { path: "test", component: PasswordTestComponent },
  {
    path: "account-recovery",
    component: AccountRecoveryComponent,
    canActivate: [LoginGuard]
  },
  {
    path: "reset-password/:token",
    component: PasswordResetComponent
  },
  { path: "confirm/:token", component: ConfirmAccountComponent },

  // otherwise redirect to home
  { path: "*", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRouting {}
