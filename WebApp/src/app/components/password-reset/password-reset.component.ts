import { Component, OnInit } from "@angular/core";
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { MyErrorStateMatcher } from "../../services/passwordmatcher.service";
import * as jwt_decode from "jwt-decode";
import { JwtHelperService } from "@auth0/angular-jwt";
import { ActivatedRoute, Router } from "@angular/router";
import { NewPasswordModel } from "../../models/new-password.model";
import { RecoveryService } from "../../services/recovery.service";
@Component({
  selector: "app-password-reset",
  templateUrl: "./password-reset.component.html",
  styleUrls: ["./password-reset.component.scss"]
})
export class PasswordResetComponent implements OnInit {
  hide = true;
  hide2 = true;
  matcher = new MyErrorStateMatcher();

  tokenAvailable: boolean = null;
  token = this.route.snapshot.params.token;
  decodedToken = jwt_decode(this.token);
  jwtHelper = new JwtHelperService();
  myForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private recoveryService: RecoveryService,
    private router: Router
  ) {
    this.myForm = this.formBuilder.group(
      {
        password: ["", [Validators.minLength(8)]],
        confirmPassword: [""]
      },
      { validator: this.checkPasswords }
    );
  }

  ngOnInit() {
    this.jwtHelper.isTokenExpired(this.token)
      ? (this.tokenAvailable = false)
      : (this.tokenAvailable = true);
  }

  getErrorMessage() {
    return this.myForm.get("password").hasError("required")
      ? "Password cannot be empty"
      : this.myForm.get("password").hasError("minlength")
      ? "Use 8 or more characters"
      : "";
  }

  checkPasswords(group: FormGroup) {
    // here we have the 'passwords' group
    const pass = group.controls.password.value;
    const confirmPass = group.controls.confirmPassword.value;
    return pass === confirmPass ? null : { notSame: true };
  }

  sendPassword() {
    const newPassword: NewPasswordModel = new NewPasswordModel(
      this.decodedToken["email"],
      this.myForm.get("password").value
    );
    this.recoveryService.newPassword(newPassword);
    setTimeout(() => {
      this.router.navigate(["/login"]);
    }, 10000);
  }
}
