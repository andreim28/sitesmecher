import { Component } from "@angular/core";

import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { UserService } from "../../services/user.service";
import { User } from "../../models/user";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent {
  hide = true;
  matcher = new MyErrorStateMatcher();

  emailFormControl = new FormControl("", [
    Validators.required,
    Validators.email
  ]);

  passwordFormControl = new FormControl("", [
    Validators.required,
    Validators.minLength(8)
  ]);

  constructor(private userService: UserService) {}

  getErrorMessage() {
    return this.passwordFormControl.hasError("required")
      ? "Password is required"
      : this.passwordFormControl.hasError("minlength")
      ? "Password must be at least 8 charahcters"
      : "";
  }

  login() {
    const user: User = new User(
      this.emailFormControl.value,
      this.passwordFormControl.value
    );
    this.userService.login(user);
    console.log(this.userService.remember);
  }
  rememberMe() {
    this.userService.rememberMe();
  }
  checked(): boolean {
    return this.userService.remember;
  }
}
