import { Component, OnInit } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { RecoveryService } from "../../services/recovery.service";
import { EmailModel } from "../../models/email.model";
import { ProblemModel } from "../../models/problem.model";
@Component({
  selector: "app-account-recovery",
  templateUrl: "./account-recovery.component.html",
  styleUrls: ["./account-recovery.component.scss"]
})
export class AccountRecoveryComponent implements OnInit {
  panelOpenStatePassword = false;
  panelOpenStateVerified = false;
  panelOpenStateBug = false;
  panelOpenStateOther = false;
  emailPassword = new FormControl("", [Validators.required, Validators.email]);
  emailConfirm = new FormControl("", [Validators.required, Validators.email]);
  emailBug = new FormControl("", [Validators.required, Validators.email]);
  emailProblem = new FormControl("", [Validators.required, Validators.email]);

  getErrorMessage(email: FormControl) {
    return email.hasError("required")
      ? "You must enter a value"
      : email.hasError("email")
      ? "Not a valid email"
      : "";
  }
  constructor(private recoveryService: RecoveryService) {}

  ngOnInit() {}
  resendEmail() {
    const email: EmailModel = new EmailModel(this.emailConfirm.value);
    this.recoveryService.resendConfirmationEmail(email);
  }
  reportBug(message: string) {
    const problem: ProblemModel = new ProblemModel(
      this.emailBug.value,
      message
    );
    this.recoveryService.reportBug(problem);
  }
  otherProblem(message: string) {
    const problem: ProblemModel = new ProblemModel(
      this.emailProblem.value,
      message
    );
    this.recoveryService.otherProblem(problem);
  }
  resetPassword() {
    const email: EmailModel = new EmailModel(this.emailPassword.value);
    this.recoveryService.resetPassword(email);
  }
}
