import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import * as jwt_decode from "jwt-decode";
import { HttpClient } from "@angular/common/http";
import { AppConfig } from "../../app.config";
import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: "app-confirm-account",
  templateUrl: "./confirm-account.component.html",
  styleUrls: ["./confirm-account.component.scss"]
})
export class ConfirmAccountComponent implements OnInit {
  isConfirmed: boolean = null;
  token = this.route.snapshot.params.token;
  decodedToken = jwt_decode(this.token);
  jwtHelper = new JwtHelperService();

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient,
    private config: AppConfig,
    private router: Router
  ) {}
  ngOnInit() {
    if (this.jwtHelper.isTokenExpired(this.token)) {
      this.isConfirmed = false;
    } else {
      return this.http
        .get(this.config.apiUrl + "confirm/" + this.decodedToken["nameid"])
        .subscribe(
          result => {
            console.log(result);
            this.isConfirmed = true;
            setTimeout(() => {
              this.router.navigate(["/login"]);
            }, 10000);
          },
          error => {
            console.log(error);
            this.isConfirmed = false;
            setTimeout(() => {
              this.router.navigate(["/account-recovery"]);
            }, 10000);
          }
        );
    }
  }
}
