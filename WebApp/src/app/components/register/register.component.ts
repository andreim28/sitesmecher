import { Component, OnInit } from "@angular/core";

import {
  FormControl,
  FormGroupDirective,
  NgForm,
  Validators
} from "@angular/forms";
import { ErrorStateMatcher } from "@angular/material/core";
import { UserService } from "../../services/user.service";
import { User } from "../../models/user";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    form: FormGroupDirective | NgForm | null
  ): boolean {
    const isSubmitted = form && form.submitted;
    return !!(
      control &&
      control.invalid &&
      (control.dirty || control.touched || isSubmitted)
    );
  }
}

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent implements OnInit {
  color: "warn";
  mode: "determinate";
  value = 33.3;
  bufferValue = 75;

  hide = true;
  matcher = new MyErrorStateMatcher();

  emailFormControl = new FormControl("", [
    Validators.required,
    Validators.minLength(8)
  ]);
  passwordFormControl = new FormControl("", [
    Validators.required,
    Validators.minLength(8)
  ]);

  getErrorMessage() {
    return this.passwordFormControl.hasError("required")
      ? "Password cannot be empty"
      : this.passwordFormControl.hasError("minlength")
      ? "Use 8 or more characters"
      : "";
  }

  constructor(private userService: UserService) {}

  ngOnInit() {}

  register() {
    const user: User = new User(
      this.emailFormControl.value,
      this.passwordFormControl.value
    );
    this.userService.register(user);
  }
}
