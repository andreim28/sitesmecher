import { Component, OnChanges, Input, SimpleChange } from "@angular/core";

@Component({
  selector: "app-password-test",
  templateUrl: "./password-test.component.html",
  styleUrls: ["./password-test.component.scss"]
})
export class PasswordTestComponent implements OnChanges {
  @Input() passwordToCheck: string;
  @Input() barLabel: string;
  bar0: string;
  bar1: string;
  bar2: string;
  bar3: string;
  bar4: string;
  showDigits = true;
  upper = true;
  lower = true;
  specialChar = true;
  uniqueLetters: number;
  showUnique = true;
  showLength = true;
  private colors = ["#F00", "#FF6666", "#F90", "#FF0", "#9F0", "#0F0"];

  private measureStrength(pass: string) {
    let score = 0;
    // award every unique letter until 5 repetitions
    const letters = {};
    this.uniqueLetters = 0;
    if (pass.length > 7) {
      this.showLength = false;
      score += 1;
    } else {
      this.showLength = true;
    }
    for (let i of pass) {
      letters[i] = (letters[i] || 0) + 1;
      if (letters[i] === 1) {
        this.uniqueLetters++;
      }
    }
    if (this.uniqueLetters < 4) {
      this.showUnique = true;
    } else {
      score += 1;
      this.showUnique = false;
    }
    // bonus points for mixing it up
    const variations = {
      digits: /\d/.test(pass),
      lower: /[a-z]/.test(pass),
      upper: /[A-Z]/.test(pass),
      nonWords: /\W/.test(pass)
    };

    if (variations.digits === true) {
      score += 1;
      this.showDigits = false;
    } else {
      this.showDigits = true;
    }
    if (variations.lower === true) {
      score += 1;
      this.upper = false;
    } else {
      this.upper = true;
    }
    if (variations.upper === true) {
      score += 1;
      this.lower = false;
    } else {
      this.lower = true;
    }
    if (variations.nonWords === true) {
      score += 1;
      this.specialChar = false;
    } else {
      this.specialChar = true;
    }
    return Math.trunc(score);
  }

  private getColor(score: number) {
    let idx = 0;
    if (score === 6) {
      idx = 5;
    } else if (score === 5) {
      idx = 4;
    } else if (score === 4) {
      idx = 3;
    } else if (score === 3) {
      idx = 2;
    } else if (score === 2) {
      idx = 1;
    } else if (score === 1) {
      idx = 0;
    }
    return {
      idx: idx + 1,
      col: this.colors[idx]
    };
  }

  ngOnChanges(changes: { [propName: string]: SimpleChange }): void {
    const password = changes["passwordToCheck"].currentValue;
    this.setBarColors(6, "#DDD");
    if (password) {
      const c = this.getColor(this.measureStrength(password));
      this.setBarColors(c.idx, c.col);
    }
  }
  private setBarColors(count, col) {
    for (let n = 0; n < count; n++) {
      this["bar" + n] = col;
    }
  }
}
