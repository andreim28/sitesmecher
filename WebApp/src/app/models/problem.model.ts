export class ProblemModel {
  Email: string;
  ProblemMessage: string;

  constructor(email, problemMessage) {
    this.Email = email;
    this.ProblemMessage = problemMessage;
  }
}
