import { Component } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import {
  Router,
  RouterEvent,
  NavigationCancel,
  NavigationEnd,
  NavigationStart,
  NavigationError
} from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "WebApp";
  spinner = true;

  constructor(private http: HttpClient, private router: Router) {
    this.router.events.subscribe((e: RouterEvent) => {
      this.navigationInterceptor(e);
    });
  }
  callApi() {
    this.http.get("https://reqres.in/api/users?page=2").subscribe(data => {
      console.log(data);
    });
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.spinner = true;
    }
    if (event instanceof NavigationEnd) {
      this.spinner = false;
    }

    // Set loading state to false in both of the below events to hide the spinner in case a request fails
    if (event instanceof NavigationCancel) {
      this.spinner = false;
    }
    if (event instanceof NavigationError) {
      this.spinner = false;
    }
  }
}
