import { Injectable } from "@angular/core";
import { EmailModel } from "../models/email.model";
import { HttpClient } from "@angular/common/http";
import { AppConfig } from "../app.config";
import { MatSnackBar } from "@angular/material";
import { ProblemModel } from "../models/problem.model";
import { NewPasswordModel } from "../models/new-password.model";

@Injectable({
  providedIn: "root"
})
export class RecoveryService {
  constructor(
    private config: AppConfig,
    private http: HttpClient,
    private snackBar: MatSnackBar
  ) {}

  resendConfirmationEmail(emailModel: EmailModel) {
    return this.http
      .post<EmailModel>(this.config.apiUrl + "Recovery/confirm", emailModel)
      .subscribe(
        response => {
          this.snackBar.open(
            "A confirmation email was sent to your adress",
            "",
            {
              duration: 10000,
              panelClass: ["purple-snackbar"],
              horizontalPosition: "end"
            }
          );
        },
        error => {
          this.snackBar.open(
            "There was a problem sending your email. Your account might not be registed or it's already confirmed",
            "",
            {
              duration: 10000,
              panelClass: ["purple-snackbar"],
              horizontalPosition: "end"
            }
          );
        }
      );
  }
  resetPassword(emailModel: EmailModel) {
    return this.http
      .post<EmailModel>(this.config.apiUrl + "Recovery/reset", emailModel)
      .subscribe(
        response => {
          this.snackBar.open(
            "A reset password email was sent to your adress",
            "",
            {
              duration: 10000,
              panelClass: ["purple-snackbar"],
              horizontalPosition: "end"
            }
          );
        },
        error => {
          this.snackBar.open(
            "There was a problem sending your email. Contact our support team",
            "",
            {
              duration: 10000,
              panelClass: ["purple-snackbar"],
              horizontalPosition: "end"
            }
          );
        }
      );
  }
  reportBug(problemModel: ProblemModel) {
    return this.http
      .post<ProblemModel>(this.config.apiUrl + "Recovery/bug", problemModel)
      .subscribe(
        response => {
          this.snackBar.open(
            "The bug was sent to our support team. You will recieve a mail soon",
            "",
            {
              duration: 10000,
              panelClass: ["purple-snackbar"],
              horizontalPosition: "end"
            }
          );
        },
        error => {
          this.snackBar.open("There was a problem sending the email", "", {
            duration: 10000,
            panelClass: ["purple-snackbar"],
            horizontalPosition: "end"
          });
        }
      );
  }
  otherProblem(problemModel: ProblemModel) {
    return this.http
      .post<ProblemModel>(this.config.apiUrl + "Recovery/problem", problemModel)
      .subscribe(
        response => {
          this.snackBar.open(
            "Your problem was sent to our support team. You will recieve a mail soon",
            "",
            {
              duration: 10000,
              panelClass: ["purple-snackbar"],
              horizontalPosition: "end"
            }
          );
        },
        error => {
          this.snackBar.open("There was a problem sending the email", "", {
            duration: 10000,
            panelClass: ["purple-snackbar"],
            horizontalPosition: "end"
          });
        }
      );
  }
  newPassword(newPasswordModel: NewPasswordModel) {
    return this.http
      .post<NewPasswordModel>(
        this.config.apiUrl + "Recovery/new-password",
        newPasswordModel
      )
      .subscribe(
        response => {
          this.snackBar.open("Your password has been changed.", "", {
            duration: 10000,
            panelClass: ["purple-snackbar"],
            horizontalPosition: "end"
          });
        },
        error => {
          this.snackBar.open("Your password has not been changed.", "", {
            duration: 10000,
            panelClass: ["purple-snackbar"],
            horizontalPosition: "end"
          });
        }
      );
  }
}
