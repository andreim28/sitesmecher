import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AppConfig } from "../app.config";
import { User } from "../models/user";
import { Router } from "@angular/router";
import { UserModel } from "../models/user.model";
import { JwtHelperService } from "@auth0/angular-jwt";
import { MatSnackBar } from "@angular/material";

@Injectable({
  providedIn: "root"
})
export class UserService {
  jwtHelper = new JwtHelperService();
  remember = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    private config: AppConfig,
    private snackBar: MatSnackBar
  ) {}

  login(currentUser: User) {
    return this.http
      .post<UserModel>(this.config.apiUrl + "User/authenticate", currentUser)
      .subscribe(
        response => {
          const user = response;
          if (this.remember === true) {
            localStorage.setItem("currentUser", JSON.stringify(user));
          } else {
            sessionStorage.setItem("currentUser", JSON.stringify(user));
          }
          this.remember = false;
          this.router.navigate([""]);
        },
        error => {
          this.snackBar.open(
            "Wrong email or password! Account might not be confirmed. Check your email",
            "",
            {
              duration: 10000,
              panelClass: ["purple-snackbar"]
            }
          );
        }
      );
  }

  logout() {
    localStorage.removeItem("currentUser");
    sessionStorage.removeItem("currentUser");
    this.router.navigate(["/login"]);
  }

  register(currentUser: User) {
    return this.http.post(this.config.apiUrl + "user", currentUser).subscribe(
      data => {
        const snackBarRef = this.snackBar.open(
          "Thanks for registering. A confirmation email was sent to your adress. You will be redirected to login in 10s",
          "Back to login",
          {
            duration: 10000,
            panelClass: ["purple-snackbar"]
          }
        );
        snackBarRef
          .onAction()
          .subscribe(() => this.router.navigate(["/login"]));
        snackBarRef
          .afterDismissed()
          .subscribe(() => this.router.navigate(["/login"]));
      },
      error => {
        const snackBarRef = this.snackBar.open(
          "Email already in use. You will be redirected to login in 10s",
          "Back to login",
          {
            duration: 10000,
            panelClass: ["purple-snackbar"]
          }
        );
        snackBarRef
          .onAction()
          .subscribe(() => this.router.navigate(["/login"]));
        snackBarRef
          .afterDismissed()
          .subscribe(() => this.router.navigate(["/login"]));
      }
    );
  }

  isTokenExpired(): boolean {
    if (localStorage.getItem("currentUser")) {
      const token = JSON.parse(localStorage.getItem("currentUser")).token;
      return this.jwtHelper.isTokenExpired(token);
    }
    return true;
  }
  isLoggedIn(): boolean {
    if (
      localStorage.getItem("currentUser") ||
      sessionStorage.getItem("currentUser")
    ) {
      return true;
    }
    return false;
  }
  rememberMe() {
    this.remember = !this.remember;
  }
}
