import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { HomeComponent } from "./components/home/home.component";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { MatFormFieldModule } from "@angular/material/form-field";
import {
  MatSelectModule,
  MatInputModule,
  MatSnackBarModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatListModule,
  MatSidenavModule,
  MatCheckboxModule
} from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";
import { FlexLayoutModule } from "@angular/flex-layout";
import { RegisterComponent } from "./components/register/register.component";
import { AppConfig } from "./app.config";
import { AuthGuard } from "./services/auth.guard";
import { UserService } from "./services/user.service";
import { AppRouting } from "./app.routing";
import { MatMenuModule } from "@angular/material/menu";
import { MatToolbarModule } from "@angular/material/toolbar";
import { AccountRecoveryComponent } from "./components/account-recovery/account-recovery.component";
import { MatExpansionModule } from "@angular/material/expansion";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { LoginGuard } from "./services/login.guard";
import { ConfirmAccountComponent } from "./components/confirm-account/confirm-account.component";
import { RecoveryService } from "./services/recovery.service";
import { PasswordResetComponent } from "./components/password-reset/password-reset.component";
import { MatSpinnerOverlayComponent } from "./components/mat-spinner-overlay/mat-spinner-overlay.component";
import { NavBarComponent } from "./components/nav-bar/nav-bar.component";

import { MatProgressBarModule } from "@angular/material/progress-bar";
import { PasswordTestComponent } from "../app/password-test/password-test.component";
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    AccountRecoveryComponent,
    ConfirmAccountComponent,
    PasswordResetComponent,
    MatSpinnerOverlayComponent,
    NavBarComponent,
    PasswordTestComponent
  ],
  imports: [
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    FlexLayoutModule,
    HttpClientModule,
    AppRouting,
    MatMenuModule,
    MatToolbarModule,
    MatExpansionModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    MatSnackBarModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatProgressBarModule
  ],
  providers: [RecoveryService, AppConfig, AuthGuard, UserService, LoginGuard],

  bootstrap: [AppComponent]
})
export class AppModule {}
