﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Services;
using WebApi.DtoModels;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace WebApi.DataAccess
{
    public class UserDataAccess : IUserDataAccess
    {
        private DataContext _context;
        private IMapper _autoMapper;
        private IPasswordHasher _passwordHasher;
        private readonly IEmailSender _emailSender;
        private readonly AppSettings _appSettings;
        private ITokenCreator _tokenCreator;

        public UserDataAccess(DataContext context, IMapper autoMapper, IPasswordHasher passwordHasher, IEmailSender emailSender, IOptions<AppSettings> appSettings, ITokenCreator tokenCreator)
        {
            _context = context;
            _autoMapper = autoMapper;
            _passwordHasher = passwordHasher;
            _emailSender = emailSender;
            _appSettings = appSettings.Value;
            _tokenCreator = tokenCreator;
        }

        public async Task<UserModel> Login(UserDtoModel user)
        {
            if (string.IsNullOrEmpty(user.Email) || string.IsNullOrEmpty(user.Password))
                return null;

            var userDbo = await _context.Users.SingleOrDefaultAsync(x => x.Email == user.Email);

            if (userDbo == null)
                return null;

            byte[] hashedPassword = _passwordHasher.EncryptPassword(Encoding.UTF8.GetBytes(user.Password), userDbo.PasswordSalt);

            if (!hashedPassword.SequenceEqual(userDbo.PasswordHash))
                return null;

            if (userDbo.Confirmed == 0)
                return null;

            return userDbo;
        }

        public async Task<UserModel> Register(UserDtoModel user)
        {
            if (string.IsNullOrWhiteSpace(user.Password))
                throw new Exception("Password is required");

            if (_context.Users.Any(x => x.Email == user.Email))
                throw new Exception("Email " + user.Email + " is already taken");

            var dboUser = _autoMapper.Map<UserModel>(user);
            dboUser.PasswordSalt = _passwordHasher.GenerateSalt();

            dboUser.PasswordHash = _passwordHasher.EncryptPassword(Encoding.UTF8.GetBytes(user.Password), dboUser.PasswordSalt);
            dboUser.Confirmed = 0;

            await _context.Users.AddAsync(dboUser);
            await _context.SaveChangesAsync();

            var tokenString = _tokenCreator.CreateToken(dboUser.UserId.ToString());

            string link = "http://localhost:4200/confirm/" + tokenString;
            string template = "Thanks for registering. To confirm your email press the following <a href = \" {0} \" > link </a> . The link will expire in 24h.";
            string message = string.Format(template, link);

            await _emailSender.SendEmailAsync(dboUser.Email, "Confirmation Email", message);

            return dboUser;
        }

        public async Task<string> Confirm(int userId)
        {
            var userDbo = await _context.Users.SingleOrDefaultAsync(x => x.UserId == userId);

            if (userDbo != null && userDbo.Confirmed == 0)
            {
                userDbo.Confirmed = 1;
                _context.SaveChanges();
                return "Email confirmed!";
            }

            throw new Exception();

        }
    }
}
