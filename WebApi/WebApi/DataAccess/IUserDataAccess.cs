﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.DtoModels;

namespace WebApi.DataAccess
{
    public interface IUserDataAccess
    {
        Task<UserModel> Register(UserDtoModel user);
        Task<UserModel> Login(UserDtoModel user);
        Task<string> Confirm(int userId);
    }
}
