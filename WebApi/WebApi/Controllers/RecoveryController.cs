﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebApi.DataAccess;
using WebApi.Models;
using WebApi.Services;
using WebApi.DtoModels;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RecoveryController : ControllerBase
    {
        private IUserDataAccess _UserDataAccess;
        private readonly AppSettings _appSettings;
        private IAccountRecovery _accountRecovery;

        public RecoveryController(
            IUserDataAccess UserDataAccess,
            IOptions<AppSettings> appSettings,
            IAccountRecovery accountRecovery)
        {
            _UserDataAccess = UserDataAccess;
            _appSettings = appSettings.Value;
            _accountRecovery = accountRecovery;
        }


        [AllowAnonymous]
        [HttpPost("reset")]
        public async Task<IActionResult> ResetPassword([FromBody]EmailDtoModel email)
        {
            try
            {
                await _accountRecovery.ResetPassword(email);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Email was not sent");
            }
        }

        [AllowAnonymous]
        [HttpPost("confirm")]
        public async Task<IActionResult> ResendConfirmationEmail([FromBody]EmailDtoModel email)
        {
            try
            {
                await _accountRecovery.ConfirmEmail(email);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Email not confirmed.Email isn't register or it's already confirmed");
            }
        }

        [AllowAnonymous]
        [HttpPost("problem")]
        public async Task<IActionResult> OtherProblem([FromBody]ProblemDtoModel problem)
        {
            try
            {
                await _accountRecovery.OtherProblem(problem);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Email not sent");
            }
        }



        [AllowAnonymous]
        [HttpPost("bug")]
        public async Task<IActionResult> ReportBug([FromBody]ProblemDtoModel problem)
        {
            try
            {
                await _accountRecovery.ReportBug(problem);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Email not sent");
            }
        }

        [AllowAnonymous]
        [HttpPost("new-password")]
        public async Task<IActionResult> NewPassword([FromBody]NewPasswordDtoModel newPasswordDtoModel)
        {
            try
            {
                await _accountRecovery.NewPassword(newPasswordDtoModel);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Email not sent");
            }
        }
    }
}