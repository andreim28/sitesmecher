﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.DataAccess;
using WebApi.Services;
using WebApi.DtoModels;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserDataAccess _UserDataAccess;
        private readonly AppSettings _appSettings;
        private ITokenCreator _tokenCreator;

        public UserController(
            IUserDataAccess UserDataAccess,
            IOptions<AppSettings> appSettings,
            ITokenCreator tokenCreator)
        {
            _UserDataAccess = UserDataAccess;
            _appSettings = appSettings.Value;
            _tokenCreator = tokenCreator;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody]UserDtoModel userModel)
        {
            var user = await _UserDataAccess.Login(userModel);

            if (user == null)
                return Unauthorized();

            var tokenString = _tokenCreator.CreateToken(user.UserId.ToString(), user.Email);

            return Ok(new
            {
                Id = user.UserId,
                Email = user.Email,
                Token = tokenString
            });
            throw new UnauthorizedAccessException();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Register([FromBody]UserDtoModel userModel)
        {
            try
            {
                await _UserDataAccess.Register(userModel);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}