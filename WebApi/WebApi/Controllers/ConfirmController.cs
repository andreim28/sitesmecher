﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebApi.DataAccess;
using WebApi.Models;
using WebApi.Services;
using WebApi.DtoModels;

namespace WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ConfirmController : ControllerBase
    {
        private IUserDataAccess _UserDataAccess;
        private readonly AppSettings _appSettings;

        public ConfirmController(
            IUserDataAccess UserDataAccess,
            IOptions<AppSettings> appSettings)
        {
            _UserDataAccess = UserDataAccess;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpGet("{userId}")]
        public async Task<IActionResult> Confirm(int userId)
        {
            try
            {
                await _UserDataAccess.Confirm(userId);
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest("Email not confirmed");
            }
        }
    }
}