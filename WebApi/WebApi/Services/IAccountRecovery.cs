﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.DtoModels;

namespace WebApi.Services
{
    public interface IAccountRecovery
    {
        Task ResetPassword(EmailDtoModel email);
        Task ConfirmEmail(EmailDtoModel email);
        Task ReportBug(ProblemDtoModel proble);
        Task OtherProblem(ProblemDtoModel proble);
        Task NewPassword(NewPasswordDtoModel newPasswordModel);
    }
}
