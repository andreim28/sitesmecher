﻿using AutoMapper;
using WebApi.Models;
using WebApi.DtoModels;

namespace WebApi.Services
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserDtoModel, UserModel>();
            CreateMap<UserModel, UserDtoModel>();
        }
    }
}
