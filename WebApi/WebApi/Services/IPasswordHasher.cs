﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services
{
    public interface IPasswordHasher
    {
        byte[] GenerateSalt();
        byte[] EncryptPassword(byte[] data, byte[] salt);
        
    }
}
