﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApi.DtoModels;

namespace WebApi.Services
{
    public class AccountRecovery : IAccountRecovery
    {
        private DataContext _context;
        private readonly IEmailSender _emailSender;
        private ITokenCreator _tokenCreator;
        private IPasswordHasher _passwordHasher;


        public AccountRecovery(DataContext context, IEmailSender emailSender, ITokenCreator tokenCreator, IPasswordHasher passwordHasher)
        {
            _context = context;
            _emailSender = emailSender;
            _tokenCreator = tokenCreator;
            _passwordHasher = passwordHasher;
        }
        public async Task ConfirmEmail(EmailDtoModel emailModel)
        {
            var userDbo = await _context.Users.SingleOrDefaultAsync(x => x.Email == emailModel.Email);
            if (userDbo.Confirmed == 1)
                throw new Exception("Account already confirmed");
            if (userDbo.Email == null)
                throw new Exception("Email adress not registered");

            var tokenString = _tokenCreator.CreateToken(userDbo.UserId.ToString());

            string link = "http://localhost:4200/confirm/" + tokenString;
            string template = "Thanks for registering. To confirm your email press the following <a href = \" {0} \" > link </a> . The link will expire in 24h.";
            string message = string.Format(template, link);

            await _emailSender.SendEmailAsync(emailModel.Email, "Confirmation Email", message);
        }

        public async Task OtherProblem(ProblemDtoModel problem)
        {
            string message = "Problem: " + problem.ProblemMessage + "<br>" + "Sender: " + problem.Email;
            await _emailSender.SendEmailAsync("testingemailsender1234@gmail.com", "Other Problem", message);
        }

        public async Task ReportBug(ProblemDtoModel problem)
        {
            string message = "Bug: " + problem.ProblemMessage + "<br>" + "Sender: " + problem.Email;
            await _emailSender.SendEmailAsync("testingemailsender1234@gmail.com", "Report Bug", message);
        }

        public async Task ResetPassword(EmailDtoModel emailModel)
        {
            var userDbo = await _context.Users.SingleOrDefaultAsync(x => x.Email == emailModel.Email);
            if (userDbo.Email == null)
                throw new Exception("Email adress not registered");

            var tokenString = _tokenCreator.CreateToken(userDbo.UserId.ToString(), userDbo.Email);

            string link = "http://localhost:4200/reset-password/" + tokenString;
            string template = "A reset password request was received.<br> If you didn't tried to change your password ignore this email.<br> Otherwise press the following email <a href = \" {0} \" > link </a> .<br> The link will expire in 24h.";
            string message = string.Format(template, link);

            await _emailSender.SendEmailAsync(emailModel.Email, "Reset password", message);
        }

        public async Task NewPassword(NewPasswordDtoModel newPasswordModel)
        {
            if (string.IsNullOrWhiteSpace(newPasswordModel.Password))
                throw new Exception("Password is required");

            var userDbo = await _context.Users.SingleOrDefaultAsync(x => x.Email == newPasswordModel.Email);

            if (userDbo.Email == null)
                throw new Exception("Email adress not registered");

            userDbo.PasswordSalt = _passwordHasher.GenerateSalt();

            userDbo.PasswordHash = _passwordHasher.EncryptPassword(Encoding.UTF8.GetBytes(newPasswordModel.Password), userDbo.PasswordSalt);

            _context.SaveChanges();

        }
    }
}
