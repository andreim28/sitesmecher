﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Services
{
    public interface ITokenCreator
    {
        string CreateToken(string userId);
        string CreateToken(string userId, string email);
    }
}
