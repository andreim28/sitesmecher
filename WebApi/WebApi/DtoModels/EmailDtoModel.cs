﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DtoModels
{
    public class EmailDtoModel
    {
        public string Email { get; set; }
    }
}
