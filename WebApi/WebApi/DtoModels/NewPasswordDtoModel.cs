﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DtoModels
{
    public class NewPasswordDtoModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
