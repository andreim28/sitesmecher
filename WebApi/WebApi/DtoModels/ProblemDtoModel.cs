﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.DtoModels
{
    public class ProblemDtoModel
    {
        public string Email { get; set; }
        public string ProblemMessage { get; set; }
    }
}
